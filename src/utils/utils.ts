import i18next from 'i18next'

export const longMessageToMany = (message: string): string[] => {
  const maxSize = 4096

  const amountSliced = Math.ceil(message.length / maxSize)
  const messagesArray = Array(amountSliced).fill('')

  let currentMessageIndex = 0
  let currentMessageSize = 0

  const tasks = message.split(i18next.t('objectBoard'))
  for (const task of tasks) {
    if (currentMessageSize + task.length > maxSize) {
      currentMessageIndex++
      currentMessageSize = 0
    }

    messagesArray[currentMessageIndex] += task
    currentMessageSize += task.length
  }

  return messagesArray
}
