import { Telegraf, type Context } from 'telegraf'
import { start } from './services/handlers/startCommand'
import { getEvents } from './services/get/getEvents'
import { getIssues } from './services/get/getIssues'
import config from './config/config'
import { getAllDomains } from './services/get/getDomains'
import { getCircleIssues } from './services/get/getCircleIssues'
import { getCircleList } from './services/get/getCircleList'
import { searchIssues } from './services/get/searchIssue'
import { menu } from './services/handlers/backMenuButtons'
import i18next from 'i18next'
import { getStagesList } from './services/get/getStagesList'
import { setIssue } from './services/set/setIssue'
import { type UserSession } from './types/task'
import { type UserSessionThought } from './types/thoughts'
import { type UserSessionTension } from './types/tensions'
import { type UserSessionDriver } from './types/drivers'
import { type UserSessionProposal } from './types/proposals'
import { type UserSessionEvent } from './types/events'
import { Domains, Priority, ActionType, IssueSearch, Commands, DriverState, ProposalStatus } from './enums/enum'
import { setPriority } from './services/handlers/priorityHadler'
import { getEventsToDay } from './services/get/getEventsToDay'
import { sendEventsToDay } from './services/set/sendEventsToDay'
import { CronJob } from 'cron'
import { getThoughts } from './services/get/getThoughts'
import { getTensions } from './services/get/getTensions'
import { getDrivers } from './services/get/getDrivers'
import { getProposals } from './services/get/getProposals'
import { getSprints } from './services/get/getSprints'
import { setEntities } from './services/handlers/EntityHandler'
import { setThought } from './services/set/setThought'
import { setTension } from './services/set/setTension'
import { setDriver } from './services/set/setDriver'
import { setProposal } from './services/set/setProposal'
import { setEvent } from './services/set/setEvent'
import { Menu, yesNoButtons } from './buttons/markups'
import { type UserSessionAI } from './types/requestAI'
import { aiRequest } from './services/requests/gigaChat'
import moment from 'moment'

process.env.TZ = config.timeZone ?? 'Europe/Moscow'

const bot = new Telegraf(config.telegramToken)

export const AIswitcher: string = config.gigaChatApiKey ? 'on' : 'off'

const userSessions: Record<string, UserSession> = {}
const userSessionsEvent: Record<string, UserSessionEvent> = {}
const userSessionsThought: Record<string, UserSessionThought> = {}
const userSessionsTension: Record<string, UserSessionTension> = {}
const userSessionsDriver: Record<string, UserSessionDriver> = {}
const userSessionsProposal: Record<string, UserSessionProposal> = {}
const userSessionsAI: Record<string, UserSessionAI> = {}
bot.start((ctx: Context) => {
  start(ctx)
})

// actions zone:
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// back & cancel section:
bot.action(/^back/, (ctx: Context) => {
  const flag: string = ActionType.back
  menu(ctx, flag)
})

bot.action(/cancel/, async (ctx: Context) => {
  const telegramId: string = ctx.from?.id.toString() ?? ''
  await ctx.editMessageText(i18next.t('cancelAction'), Menu())
  userSessions[telegramId] = {
    action: ActionType.noAction,
    issueName: '',
    domainId: '',
    stageId: '',
    priority: Priority.MediumPriority
  }
  userSessionsThought[telegramId] = {
    action: ActionType.noAction,
    thoughtName: '',
    domainId: ''
  }
  userSessionsTension[telegramId] = {
    action: ActionType.noAction,
    tensionName: '',
    description: '',
    tensionMark: '',
    domainId: ''
  }
  userSessionsDriver[telegramId] = {
    action: ActionType.noAction,
    driverName: '',
    description: '',
    driverEffect: '',
    driverState: DriverState.defult,
    domainId: ''
  }
  userSessionsProposal[telegramId] = {
    action: ActionType.noAction,
    proposalName: '',
    description: '',
    status: ProposalStatus.default,
    driverIds: [],
    domainId: ''
  }
  userSessionsAI[telegramId] = {
    action: ActionType.noAction,
    requestText: ''
  }
})

// ----get section-----
bot.action('getIssueList', (ctx: Context) => {
  const currentPage: number = 1
  getIssues(ctx, currentPage)
})

bot.action('getEvents', (ctx: Context) => {
  const currentPage: number = 1
  getEvents(ctx, currentPage)
})

bot.action('getEventsToDay', (ctx: Context) => {
  const currentPage: number = 1
  getEventsToDay(ctx, currentPage)
})

const job = new CronJob(config.todayEventsTime ?? '4 0 * * *', async () => {
  // const currentPage: number = 1;
  await sendEventsToDay(bot, config.chatID, config.subChatID)
})

job.start()

bot.action('getDomains', (ctx: Context) => {
  getAllDomains(ctx)
})

bot.action('getCircleList', (ctx: Context) => {
  const flag: string = Domains.getDomains
  getCircleList(ctx, flag)
})

bot.action(/^domain:/, (ctx: Context) => {
  const domainID: string = (ctx.callbackQuery as any).data as string
  const domainId: string = domainID.replace('domain:', '')
  const currentPage: number = 1
  getCircleIssues(domainId, currentPage, ctx)
})

bot.action('getThoughts', (ctx: Context) => {
  const currentPage = 1
  getThoughts(ctx, currentPage)
})

bot.action('getTensions', (ctx: Context) => {
  const currentPage = 1
  getTensions(ctx, currentPage)
})

bot.action('getDrivers', (ctx: Context) => {
  const currentPage = 1
  getDrivers(ctx, currentPage)
})

bot.action('getProposals', (ctx: Context) => {
  const currentPage = 1
  getProposals(ctx, currentPage)
})

bot.action('getSprints', (ctx: Context) => {
  const currentPage = 1
  getSprints(ctx, currentPage)
})

// scroll action zone:
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// for domainIssues
bot.action(/^scrollPage:/, async (ctx: Context) => {
  const callbackData = (ctx.callbackQuery as any).data as string
  const parts = callbackData.split('domainId:')
  const scrollPageData = parts[0].split(':')[1]
  const domainId = parts[1]
  const currentPage: string = scrollPageData
  await getCircleIssues(domainId, currentPage, ctx)
})
// for AllIssues
bot.action(/^scrolPage:(\d+)/, async (ctx: Context) => {
  const callbackData = (ctx.callbackQuery as any).data as string
  const currentPage = parseInt(callbackData.split(':')[1])
  await getIssues(ctx, currentPage)
})
// for AllEvents
bot.action(/^scrollEvent:(\d+)/, async (ctx: Context) => {
  const callbackData = (ctx.callbackQuery as any).data as string
  const currentPage = parseInt(callbackData.split(':')[1])
  await getEvents(ctx, currentPage)
})
// for events today
bot.action(/^scrollEventToDay:(\d+)/, async (ctx: Context) => {
  const callbackData = (ctx.callbackQuery as any).data as string
  const currentPage = parseInt(callbackData.split(':')[1])
  await getEventsToDay(ctx, currentPage)
})
// for searching issues
bot.action(/^scrollSearchedIssues:(\d+)/, async (ctx: Context) => {
  const callbackData = (ctx.callbackQuery as any).data as string
  const parts = callbackData.split('task:')
  const scrollPageData = parts[0].split(':')[1]
  const task: string = Buffer.from(parts[1], 'base64').toString('utf-8')
  const currentPage: string = scrollPageData
  const flag: string = IssueSearch.currentPage

  await searchIssues(ctx, task, flag, currentPage)
})
// for thouhts
bot.action(/^scrollThoughts:(\d+)/, async (ctx: Context) => {
  const callbackData = (ctx.callbackQuery as any).data as string
  const currentPage = parseInt(callbackData.split(':')[1])
  await getThoughts(ctx, currentPage)
})
// for tensions
bot.action(/^scrollTensions:(\d+)/, async (ctx: Context) => {
  const callbackData = (ctx.callbackQuery as any).data as string
  const currentPage = parseInt(callbackData.split(':')[1])
  await getTensions(ctx, currentPage)
})
// for drivers
bot.action(/^scrollDrivers:(\d+)/, async (ctx: Context) => {
  const callbackData = (ctx.callbackQuery as any).data as string
  const currentPage = parseInt(callbackData.split(':')[1])
  await getDrivers(ctx, currentPage)
})
// for proposals
bot.action(/^scrollProposals:(\d+)/, async (ctx: Context) => {
  const callbackData = (ctx.callbackQuery as any).data as string
  const currentPage = parseInt(callbackData.split(':')[1])
  await getProposals(ctx, currentPage)
})
// for sprints
bot.action(/^scrollSprints:(\d+)/, async (ctx: Context) => {
  const callbackData = (ctx.callbackQuery as any).data as string
  const currentPage = parseInt(callbackData.split(':')[1])
  await getSprints(ctx, currentPage)
})

// --------------end of Scroll zone-------------------------------
// issue
bot.action('setIssue', async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const telegramId = ctx.from?.id ?? ''
    const flag = ActionType.addIssue
    userSessions[telegramId] = {
      action: ActionType.addIssue,
      issueName: '',
      domainId: '',
      stageId: '',
      priority: Priority.MediumPriority
    }
    setEntities(ctx, flag)
  }
})
// set Entities zone:

bot.action(/^setDomain:/, (ctx: Context) => {
  const domainID: string = (ctx.callbackQuery as any).data as string
  const domainId: string = domainID.replace('setDomain:', '')
  const telegramId: string = ctx.from?.id.toString() ?? ''
  userSessions[telegramId] = {
    ...userSessions[telegramId],
    domainId
  }
  getStagesList(ctx, domainId)
})

bot.action(/^setStage:/, (ctx: Context) => {
  const stageID: string = (ctx.callbackQuery as any).data as string
  const stageId: string = stageID.replace('setStage:', '')
  const telegramId: string = ctx.from?.id.toString() ?? ''
  userSessions[telegramId] = {
    ...userSessions[telegramId],
    stageId
  }
  setPriority(ctx)
})

bot.action(/^setPriority:/, (ctx: Context) => {
  const callBackPriority: string = (ctx.callbackQuery as any).data as string
  const priority: number = Number(callBackPriority.replace('setPriority:', ''))
  const telegramId: string = ctx.from?.id.toString() ?? ''
  userSessions[telegramId] = {
    ...userSessions[telegramId],
    priority
  }
  setIssue(ctx, userSessions[telegramId])
  userSessions[telegramId] = {
    action: ActionType.noAction,
    issueName: '',
    domainId: '',
    stageId: '',
    priority: Priority.MediumPriority
  }
})

bot.action('setEntity', (ctx: Context) => {
  const flag = ActionType.choice
  setEntities(ctx, flag)
})
// Events
bot.action('setEvent', async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const telegramId = ctx.from?.id ?? ''
    const flag = ActionType.addEvent
    userSessionsEvent[telegramId] = {
      action: ActionType.addEvent,
      eventName: '',
      startDate: '',
      endDate: '',
      domainId: ''
    }
    setEntities(ctx, flag)
  }
})

bot.action(/^add_event:/, (ctx: Context) => {
  const domainID: string = (ctx.callbackQuery as any).data as string
  const domainId: string = domainID.replace('add_event:', '')
  const telegramId: string = ctx.from?.id.toString() ?? ''
  userSessionsEvent[telegramId] = {
    ...userSessionsEvent[telegramId],
    domainId
  }
  setEvent(ctx, userSessionsEvent[telegramId])
  userSessionsEvent[telegramId] = {
    action: ActionType.noAction,
    eventName: '',
    startDate: '',
    endDate: '',
    domainId: ''
  }
})

bot.action('yes', async (ctx: Context) => {
  const telegramId: string = ctx.from?.id.toString() ?? ''
  setEvent(ctx, userSessionsEvent[telegramId])
  console.log('userSessionEVENT------->', userSessionsEvent[telegramId])
  userSessionsEvent[telegramId] = {
    action: ActionType.noAction,
    eventName: '',
    startDate: '',
    endDate: '',
    domainId: ''
  }
})

// Thoughts
bot.action('setThought', async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const telegramId = ctx.from?.id ?? ''
    const flag = ActionType.addThought
    userSessionsThought[telegramId] = {
      action: ActionType.addThought,
      thoughtName: '',
      domainId: ''
    }
    setEntities(ctx, flag)
  }
})

bot.action(/^add_thought:/, (ctx: Context) => {
  const domainID: string = (ctx.callbackQuery as any).data as string
  const domainId: string = domainID.replace('add_thought:', '')
  const telegramId: string = ctx.from?.id.toString() ?? ''
  userSessionsThought[telegramId] = {
    ...userSessionsThought[telegramId],
    domainId
  }
  setThought(ctx, userSessionsThought[telegramId])
  userSessionsThought[telegramId] = {
    action: ActionType.noAction,
    thoughtName: '',
    domainId: ''
  }
})
// Tensions
bot.action('setTension', async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const telegramId = ctx.from?.id ?? ''
    const flag = ActionType.addTension
    userSessionsTension[telegramId] = {
      action: ActionType.addTension,
      tensionName: '',
      description: '',
      tensionMark: '',
      domainId: ''
    }
    setEntities(ctx, flag)
  }
}
)

bot.action(/^gradeTension:/, (ctx: Context) => {
  const callBack = (ctx.callbackQuery as any).data as string
  const tensionMark = callBack.replace('gradeTension:', '')
  const telegramId: string = ctx.from?.id.toString() ?? ''
  const flag = 'add_tension'
  userSessionsTension[telegramId] = {
    ...userSessionsTension[telegramId],
    tensionMark
  }
  getCircleList(ctx, flag)
})

bot.action(/^add_tension:/, (ctx: Context) => {
  const domainID: string = (ctx.callbackQuery as any).data as string
  const domainId: string = domainID.replace('add_tension:', '')
  const telegramId: string = ctx.from?.id.toString() ?? ''
  userSessionsTension[telegramId] = {
    ...userSessionsTension[telegramId],
    domainId
  }
  setTension(ctx, userSessionsTension[telegramId])
  userSessionsTension[telegramId] = {
    action: ActionType.noAction,
    tensionName: '',
    description: '',
    tensionMark: '',
    domainId: ''
  }
})

// driver
bot.action('setDriver', async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const telegramId = ctx.from?.id ?? ''
    const flag = ActionType.addDriver
    userSessionsDriver[telegramId] = {
      action: ActionType.addDriver,
      driverName: '',
      description: '',
      driverEffect: '',
      driverState: DriverState.defult,
      domainId: ''
    }
    setEntities(ctx, flag)
  }
}
)

bot.action(/^gradeDriver:/, (ctx: Context) => {
  const callBack = (ctx.callbackQuery as any).data as string
  const driverEffect = callBack.replace('gradeDriver:', '')
  const telegramId: string = ctx.from?.id.toString() ?? ''
  const flag = 'driverState'
  userSessionsDriver[telegramId] = {
    ...userSessionsDriver[telegramId],
    driverEffect
  }
  setEntities(ctx, flag)
})

bot.action(/^stateDriver:/, (ctx: Context) => {
  const callBack = (ctx.callbackQuery as any).data as string
  const stateDriver = callBack.replace('stateDriver:', '')
  const driverState = Object.values(DriverState).find(state => state === stateDriver)

  if (driverState) {
    const telegramId: string = ctx.from?.id.toString() ?? ''
    const flag = 'add_driver'
    userSessionsDriver[telegramId] = {
      ...userSessionsDriver[telegramId],
      driverState
    }
    getCircleList(ctx, flag)
  } else {
    ctx.reply('badDriverState')
  }
})

bot.action(/^add_driver:/, (ctx: Context) => {
  const domainID: string = (ctx.callbackQuery as any).data as string
  const domainId: string = domainID.replace('add_driver:', '')
  const telegramId: string = ctx.from?.id.toString() ?? ''
  userSessionsDriver[telegramId] = {
    ...userSessionsDriver[telegramId],
    domainId
  }
  setDriver(ctx, userSessionsDriver[telegramId])
  userSessionsDriver[telegramId] = {
    action: ActionType.noAction,
    driverName: '',
    description: '',
    driverEffect: '',
    driverState: DriverState.defult,
    domainId: ''
  }
})

// proposal:

bot.action('setProposal', async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const telegramId = ctx.from?.id ?? ''
    const flag = ActionType.addProposal
    userSessionsProposal[telegramId] = {
      action: ActionType.addProposal,
      proposalName: '',
      description: '',
      status: ProposalStatus.default,
      driverIds: [],
      domainId: ''
    }
    setEntities(ctx, flag)
  }
}
)

bot.action(/^proposalStatus:/, (ctx: Context) => {
  const callback = (ctx.callbackQuery as any).data as string
  const Status = callback.replace('proposalStatus:', '')
  const proposalStatus = Object.values(ProposalStatus).find(status => status === Status)
  if (proposalStatus) {
    const telegramId: string = ctx.from?.id.toString() ?? ''
    const flag = 'add_proposal'
    userSessionsProposal[telegramId] = {
      ...userSessionsProposal[telegramId],
      status: proposalStatus
    }
    getCircleList(ctx, flag)
  } else {
    ctx.reply('badProposalStatus')
  }
})

bot.action(/^add_proposal:/, (ctx: Context) => {
  const domainID: string = (ctx.callbackQuery as any).data as string
  const domainId: string = domainID.replace('add_proposal:', '')
  const telegramId: string = ctx.from?.id.toString() ?? ''
  userSessionsProposal[telegramId] = {
    ...userSessionsProposal[telegramId],
    domainId
  }
  setProposal(ctx, userSessionsProposal[telegramId])
  userSessionsProposal[telegramId] = {
    action: ActionType.noAction,
    proposalName: '',
    description: '',
    status: ProposalStatus.default,
    driverIds: [],
    domainId: ''
  }
})

// commands zone:
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// add part ----------------

bot.command(/add_issue/, async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const messageText: string = (ctx.message as any).text
    const taskMatch: RegExpMatchArray | null = messageText.match(/\/add_issue\s+(.+)/)
    if (taskMatch === null) {
      await ctx.reply(i18next.t('noNameIssue'))
    } else {
      const task: string = taskMatch[1]
      const flag: string = ActionType.addIssue
      const telegramId: string = ctx.from?.id.toString() ?? ''
      userSessions[telegramId] = {
        ...userSessions[telegramId],
        action: ActionType.addIssue,
        issueName: task
      }
      getCircleList(ctx, flag)
    }
  }
})

bot.command(/add_event/, async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const messageText: string = (ctx.message as any).text
    const eventMatch: RegExpMatchArray | null = messageText.match(/\/add_event\s+(.+)/)
    if (eventMatch === null) {
      await ctx.reply(i18next.t('noNameEvent'))
    } else {
      const event: string = eventMatch[1]
      const telegramId: string = ctx.from?.id.toString() ?? ''
      if (AIswitcher === 'off') {
        const flag: string = ActionType.addEvent + '_command'
        console.log(`Flag----->${flag}`)
        userSessionsEvent[telegramId] = {
          action: ActionType.addEvent,
          eventName: event,
          startDate: '',
          endDate: '',
          domainId: ''
        }
        setEntities(ctx, flag)
      }
      if (AIswitcher === 'on') {
        const now = new Date()
        const timeNow = moment(now).format('DD.MM.yyyy hh:mm')
        const daysOfWeek = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']
        const weekDay = daysOfWeek[now.getDay()]
        console.log(`Сегодня: ${weekDay}`)
        const userText: string = event
        const requestText: string = i18next.t('addEventFromAI', { timeNow, weekDay, userText })
        console.log('request text here---->', requestText)
        userSessionsAI[telegramId] = {
          action: ActionType.addAIRequest,
          requestText
        }
        console.log('ВЫВОД------->', i18next.t('addEventFromAI', { timeNow, weekDay, userText }))
        const roleContent: string = i18next.t('AiContentGetDate')
        console.log(`addEventFromAI--->${typeof timeNow}`)
        console.log(roleContent)
        const eventString: string = await aiRequest(ctx, userSessionsAI[telegramId], roleContent) ?? ''
        const eventJSon = JSON.parse(eventString)
        console.log('eventJSON----------->', eventJSon)
        const eventName: string = eventJSon.title
        const startDate: string = eventJSon.date
        const [datePart, timePart] = startDate.split(' ')
        const [hours, minutes] = timePart.split(':')
        const newHours = String(parseInt(hours, 10) + 2).padStart(2, '0')
        const endDate: string = `${datePart} ${newHours}:${minutes}`
        console.log('endDate------------->', endDate)
        userSessionsEvent[telegramId] = {
          action: ActionType.addEvent,
          eventName,
          startDate,
          endDate,
          domainId: config.defaultDomainId
        }
        const validText: string = `Правильно ли я тебя понял, проверь пожалуйста:\n\nСобытие: ${eventName}\nДата и время начала: ${startDate}`
        ctx.reply(validText, yesNoButtons())
        userSessionsAI[telegramId] = {
          action: ActionType.noAction,
          requestText: ''
        }
      }
    }
  }
})

bot.command(/add_thought/, async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const messageText: string = (ctx.message as any).text
    const thoughtMatch: RegExpMatchArray | null = messageText.match(/\/add_thought\s+(.+)/)
    if (thoughtMatch === null) {
      await ctx.reply(i18next.t('noNameThought'))
    } else {
      const thought: string = thoughtMatch[1]
      const flag: string = ActionType.addThought
      const telegramId: string = ctx.from?.id.toString() ?? ''
      userSessionsThought[telegramId] = {
        ...userSessionsThought[telegramId],
        action: ActionType.addThought,
        thoughtName: thought
      }
      getCircleList(ctx, flag)
    }
  }
})

bot.command(/add_tension/, async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const messageText: string = (ctx.message as any).text
    const tensionMatch: RegExpMatchArray | null = messageText.match(/\/add_tension\s+(.+)/)
    if (tensionMatch === null) {
      await ctx.reply(i18next.t('noNameTension'))
    } else {
      const tension: string = tensionMatch[1]
      const flag: string = 'description'
      const telegramId: string = ctx.from?.id.toString() ?? ''
      userSessionsTension[telegramId] = {
        ...userSessionsTension[telegramId],
        action: ActionType.addTension,
        tensionName: tension
      }
      setEntities(ctx, flag)
    }
  }
})

bot.command(/add_driver/, async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const messageText: string = (ctx.message as any).text
    const driverMatch: RegExpMatchArray | null = messageText.match(/\/add_driver\s+(.+)/)
    if (driverMatch === null) {
      await ctx.reply(i18next.t('noNameDriver'))
    } else {
      const driverName: string = driverMatch[1]
      console.log(driverName)
      const flag: string = 'description'
      const telegramId: string = ctx.from?.id.toString() ?? ''
      userSessionsDriver[telegramId] = {
        ...userSessionsDriver[telegramId],
        action: ActionType.addDriver,
        driverName
      }
      setEntities(ctx, flag)
    }
  }
})

bot.command(/add_proposal/, async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const messageText: string = (ctx.message as any).text
    const proposalMatch: RegExpMatchArray | null = messageText.match(/\/add_proposal\s+(.+)/)
    if (proposalMatch === null) {
      await ctx.reply(i18next.t('noNameProposal'))
    } else {
      const proposalName: string = proposalMatch[1]
      console.log(`proposalNAme----->${proposalName}`)
      const flag: string = 'description'
      const telegramId: string = ctx.from?.id.toString() ?? ''
      userSessionsProposal[telegramId] = {
        ...userSessionsProposal[telegramId],
        action: ActionType.addProposal,
        proposalName
      }
      setEntities(ctx, flag)
    }
  }
})

bot.command(/ai_ask/, async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const userText: string = (ctx.message as any).text
    const telegramId = ctx.from?.id ?? ''
    userSessionsAI[telegramId] = {
      action: ActionType.addAIRequest,
      requestText: userText
    }
    const roleContent: string = i18next.t('AiContentAssistent')
    aiRequest(ctx, userSessionsAI[telegramId], roleContent)
    userSessionsAI[telegramId] = {
      action: ActionType.noAction,
      requestText: ''
    }
  }
})

// help command----------------
bot.command(/help/, async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const commandList = Object.values(Commands)
    const avalibleCommands = commandList.join('\n')
    const newLine = '\n'
    await ctx.reply(i18next.t('HelpCommandText', { avalibleCommands, newLine, interpolation: { escapeValue: false } }), Menu())
  }
})

// search part-------------------
bot.command(/issue_search/, async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const messageText: string = (ctx.message as any).text
    const taskMatch: RegExpMatchArray | null = messageText.match(/\/issue_search\s+(.+)/)
    if (taskMatch === null) {
      await ctx.reply(i18next.t('noNameIssue'))
    } else {
      const task: string = taskMatch[1]
      const currentPage: number = 1
      const flag: string = IssueSearch.firstPage

      searchIssues(ctx, task, flag, currentPage)
    }
  }
})

// bot.on handler zone:
// ------------
bot.on('text', async (ctx: Context) => {
  const telegramId = ctx.from?.id ?? ''
  const issueSession = userSessions[telegramId]
  const eventSession = userSessionsEvent[telegramId]
  const thoughtSession = userSessionsThought[telegramId]
  const tensionSession = userSessionsTension[telegramId]
  const driverSession = userSessionsDriver[telegramId]
  const proposalSession = userSessionsProposal[telegramId]
  const AISession = userSessionsAI[telegramId]

  console.log('issueSession:', issueSession)
  console.log('Event Session:', eventSession)
  console.log('Thought Session:', thoughtSession)
  console.log('Tension Session:', tensionSession)
  console.log('Driver Session:', driverSession)
  console.log('Proposal Session:', proposalSession)
  console.log('AI Session:', AISession)

  if (issueSession && issueSession.action === ActionType.addIssue) {
    if (ctx.message && 'text' in ctx.message) {
      if (issueSession.issueName === '') {
        const issueName: string = ctx.message?.text ?? ''
        const flag: string = ActionType.addIssue
        userSessions[telegramId] = {
          ...userSessions[telegramId],
          issueName
        }
        await getCircleList(ctx, flag)
      }
    }
  } else if (eventSession && eventSession.action === ActionType.addEvent) {
    if (ctx.message && 'text' in ctx.message) {
      if (eventSession.eventName === '') {
        const eventName: string = ctx.message?.text ?? ''
        let flag = ''
        if (AIswitcher === 'on') {
          // flag = 'startDateAI'
          flag = 'startDate'
        }
        if (AIswitcher === 'off') {
          flag = 'startDate'
        }
        userSessionsEvent[telegramId] = {
          ...userSessionsEvent[telegramId],
          eventName
        }
        setEntities(ctx, flag)
      } else if (eventSession.startDate === '') {
        const startDate = ctx.message?.text ?? ''
        console.log('start Date: ', startDate)
        const flag = 'add_event'
        userSessionsEvent[telegramId] = {
          ...userSessionsEvent[telegramId],
          startDate
        }
        await getCircleList(ctx, flag)
      }
    }
  } else if (thoughtSession && thoughtSession.action === ActionType.addThought) {
    if (ctx.message && 'text' in ctx.message) {
      if (thoughtSession.thoughtName === '') {
        const thoughtName: string = ctx.message?.text ?? ''
        const flag: string = ActionType.addThought
        userSessionsThought[telegramId] = {
          ...userSessionsThought[telegramId],
          thoughtName
        }
        await getCircleList(ctx, flag)
      }
    }
  } else if (tensionSession && tensionSession.action === ActionType.addTension) {
    if (ctx.message && 'text' in ctx.message) {
      if (tensionSession.tensionName === '') {
        const tensionName: string = ctx.message?.text ?? ''
        const flag: string = 'description'
        userSessionsTension[telegramId] = {
          ...userSessionsTension[telegramId],
          tensionName
        }
        setEntities(ctx, flag)
      } else if (tensionSession.description === '') {
        const flag = 'tensionMark'
        const description: string = ctx.message?.text ?? ''
        userSessionsTension[telegramId] = {
          ...userSessionsTension[telegramId],
          description
        }
        setEntities(ctx, flag)
      }
    }
  } else if (driverSession && driverSession.action === ActionType.addDriver) {
    if (ctx.message && 'text' in ctx.message) {
      if (driverSession.driverName === '') {
        const driverName: string = ctx.message?.text ?? ''
        const flag: string = 'description'
        userSessionsDriver[telegramId] = {
          ...userSessionsDriver[telegramId],
          driverName
        }
        setEntities(ctx, flag)
      } else if (driverSession.description === '') {
        const flag = 'driverEffect'
        const description: string = ctx.message?.text ?? ''
        userSessionsDriver[telegramId] = {
          ...userSessionsDriver[telegramId],
          description
        }
        setEntities(ctx, flag)
      }
    }
  } else if (proposalSession && proposalSession.action === ActionType.addProposal) {
    if (ctx.message && 'text' in ctx.message) {
      if (proposalSession.proposalName === '') {
        const proposalName: string = ctx.message?.text ?? ''
        const flag: string = 'description'
        userSessionsProposal[telegramId] = {
          ...userSessionsProposal[telegramId],
          proposalName
        }
        setEntities(ctx, flag)
      } else if (proposalSession.description === '') {
        const flag = 'status'
        const description: string = ctx.message?.text ?? ''
        userSessionsProposal[telegramId] = {
          ...userSessionsProposal[telegramId],
          description
        }
        setEntities(ctx, flag)
      }
    }
  } else if (AISession && AISession.action === ActionType.addAIRequest) {
    if (ctx.message && 'text' in ctx.message) {
      if (AISession.requestText === '') {
        const requestText: string = ctx.message?.text ?? ''
        userSessionsAI[telegramId] = {
          ...userSessionsAI[telegramId],
          requestText
        }
        const roleContent: string = i18next.t('AiContentAssistent')
        aiRequest(ctx, userSessionsAI[telegramId], roleContent)
        userSessionsAI[telegramId] = {
          action: ActionType.noAction,
          requestText: ''
        }
      }
    }
  } else {
    if (ctx.message && 'text' in ctx.message) {
      const messageText = ctx.message.text
      const commandList = Object.values(Commands)
      const isCommand = commandList.some(command => messageText.startsWith(command))
      if (!isCommand) {
        console.log('User text input')
      }
    }
  }
}
)

// ----------------------------
// AI requests zone:
bot.action('askToAI', async (ctx: Context) => {
  if (config.isDemo) {
    await ctx.reply(i18next.t('demo'))
  } else {
    const telegramId = ctx.from?.id ?? ''
    const flag = ActionType.addAIRequest
    userSessionsAI[telegramId] = {
      action: ActionType.addAIRequest,
      requestText: ''
    }
    setEntities(ctx, flag)
  }
}
)
// -----------------------------------------------------
// finish zone:
// ------------

bot.launch().then(() => {
}).catch((err) => {
  console.error('Error starting bot', err)
})
