// import { issueType } from "./issues"

export interface sprintType {
  id: string
  name: string
  goal: string
  startDate: string
  endDate: string
  participants: Array<{
    name: string
  }>
  issues: Array<{
    id: string
    name: string
  }>
  // issues: issueType[]
  domain: {
    id: string
    name: string
  }
}
