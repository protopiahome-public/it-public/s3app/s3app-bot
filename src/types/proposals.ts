import { type ProposalStatus } from '../enums/enum'

export interface proposalType {
  id: string
  name: string
  authorUser: {
    name: string
  } | null
  status: ProposalStatus
  domain: {
    id: string
    name: string
  }
  createdAt: string
  updatedAt: string | null
}

export interface UserSessionProposal {
  action: string
  proposalName: string
  description: string
  status: ProposalStatus
  driverIds: []
  domainId: string
}
