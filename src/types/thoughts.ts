export interface thoughtType {
  id: string
  domain: {
    id: string
    name: string
  }
  name: string
  authorUser: {
    name: string
  } | null
  createdAt: string
  updatedAt: string | null
}

export interface UserSessionThought {
  action: string
  thoughtName: string
  domainId: string
}
