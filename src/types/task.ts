export interface UserSession {
  action: string
  issueName: string
  domainId: string
  stageId: string
  priority: number
}
