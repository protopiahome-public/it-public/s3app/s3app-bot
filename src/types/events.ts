export interface eventsType {
  id: string
  name: string
  domain: {
    id: string
    name: string
  }
  startDate: string
  endDate: string
  participants: Array<{
    name: string
  }>
}

export interface UserSessionEvent {
  action: string
  eventName: string
  startDate: string
  endDate: string
  domainId: string
}
