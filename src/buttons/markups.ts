import { type InlineKeyboardMarkup } from 'telegraf/typings/core/types/typegram'
import i18next from '../localization/phrasesRus'
import { DriverState, Grade, Priority, ProposalStatus } from '../enums/enum'

export function menuButtons (): { reply_markup: InlineKeyboardMarkup } {
  const button = {
    reply_markup: {
      inline_keyboard: [
        [{ text: i18next.t('getIssueList'), callback_data: 'getIssueList' }],
        [{ text: i18next.t('getDomains'), callback_data: 'getDomains' }],
        [{ text: i18next.t('getCircleList'), callback_data: 'getCircleList' }],
        [{ text: i18next.t('getEvents'), callback_data: 'getEvents' }],
        [{ text: i18next.t('getEventsToDay'), callback_data: 'getEventsToDay' }],
        [{ text: i18next.t('getThoughtsList'), callback_data: 'getThoughts' }],
        [{ text: i18next.t('getTensionsList'), callback_data: 'getTensions' }],
        [{ text: i18next.t('getDriversList'), callback_data: 'getDrivers' }],
        [{ text: i18next.t('getProposalsList'), callback_data: 'getProposals' }],
        [{ text: i18next.t('getSprintsList'), callback_data: 'getSprints' }],
        [{ text: i18next.t('setEntity'), callback_data: 'setEntity' }]
        // [{ text: i18next.t('askToAI'), callback_data: 'askToAI' }]
      ],
      resize_keyboard: true
    }
  }
  return button
}
export function Menu (): { reply_markup: InlineKeyboardMarkup } {
  const button = {
    reply_markup: {
      inline_keyboard: [
        [{ text: i18next.t('Menu'), callback_data: 'back' }]
      ],
      resize_keyboard: true
    }
  }
  return button
}

export function circleButtons (domainData?: any): { reply_markup: InlineKeyboardMarkup } {
  const button = {
    reply_markup: {
      inline_keyboard: domainData,
      resize_keyboard: true
    }
  }
  return button
}

export function domainButtons (domainData?: any): { reply_markup: InlineKeyboardMarkup } {
  const cancelButton = [{ text: i18next.t('cancel'), callback_data: 'cancel' }]
  const inlineKeyboard = [...domainData, cancelButton]
  const button = {
    reply_markup: {
      inline_keyboard: inlineKeyboard,
      resize_keyboard: true
    }
  }
  return button
}

export function stagesButtons (stagesList?: any): { reply_markup: InlineKeyboardMarkup } {
  const cancelButton = [{ text: i18next.t('cancel'), callback_data: 'cancel' }]
  const inlineKeyboard = [...stagesList, cancelButton]
  const button = {
    reply_markup: {
      inline_keyboard: inlineKeyboard,
      resize_keyboard: true
    }
  }
  return button
}

export function setEntitiesMenuButtons (): { reply_markup: InlineKeyboardMarkup } {
  const button = {
    reply_markup: {
      inline_keyboard: [
        [{ text: i18next.t('getIssueList'), callback_data: 'setIssue' }],
        [{ text: i18next.t('getEvents'), callback_data: 'setEvent' }],
        [{ text: i18next.t('getThoughtsList'), callback_data: 'setThought' }],
        [{ text: i18next.t('getTensionsList'), callback_data: 'setTension' }],
        [{ text: i18next.t('getDriversList'), callback_data: 'setDriver' }],
        [{ text: i18next.t('getProposalsList'), callback_data: 'setProposal' }],
        [{ text: i18next.t('back'), callback_data: 'back' }]
      ],
      resize_keyboard: true
    }
  }
  return button
}

export function cancelButton (): { reply_markup: InlineKeyboardMarkup } {
  const button = {
    reply_markup: {
      inline_keyboard: [
        [{ text: i18next.t('cancel'), callback_data: 'cancel' }]
      ],
      resize_keyboard: true
    }
  }
  return button
}

// ---------------scroll buttons zone-------------
export function scrollButtons (currentPage: number | string, totalPages: number): Array<{ text: string, callback_data: string }> {
  currentPage = +currentPage
  const buttons = []
  if (currentPage > 1) {
    buttons.push({ text: i18next.t('toHome'), callback_data: `scrolPage:${1}` })
    buttons.push({ text: i18next.t('left'), callback_data: `scrolPage:${currentPage - 1}` })
  }
  if (currentPage < totalPages) {
    buttons.push({ text: i18next.t('right'), callback_data: `scrolPage:${currentPage + 1}` })
    buttons.push({ text: i18next.t('toEnd'), callback_data: `scrolPage:${totalPages}` })
  }
  buttons.push({ text: i18next.t('back'), callback_data: 'back' })
  return buttons
}

export function scrollButtonsDomainIssues (currentPage: number | string, totalPages: number, domainID: string): Array<{ text: string, callback_data: string }> {
  currentPage = +currentPage
  const buttons = []
  if (currentPage > 1) {
    buttons.push({ text: i18next.t('toHome'), callback_data: `scrollPage:${1}domainId:${domainID}` })
    buttons.push({ text: i18next.t('left'), callback_data: `scrollPage:${currentPage - 1}domainId:${domainID}` })
  }
  if (currentPage < totalPages) {
    buttons.push({ text: i18next.t('right'), callback_data: `scrollPage:${currentPage + 1}domainId:${domainID}` })
    buttons.push({ text: i18next.t('toEnd'), callback_data: `scrollPage:${totalPages}domainId:${domainID}` })
  }
  buttons.push({ text: i18next.t('back'), callback_data: 'back' })
  return buttons
}

export function scrollButtonsEvents (currentPage: number | string, totalPages: number): Array<{ text: string, callback_data: string }> {
  currentPage = +currentPage
  const buttons = []
  if (currentPage > 1) {
    buttons.push({ text: i18next.t('toHome'), callback_data: `scrollEvent:${1}` })
    buttons.push({ text: i18next.t('left'), callback_data: `scrollEvent:${currentPage - 1}` })
  }
  if (currentPage < totalPages) {
    buttons.push({ text: i18next.t('right'), callback_data: `scrollEvent:${currentPage + 1}` })
    buttons.push({ text: i18next.t('toEnd'), callback_data: `scrollEvent:${totalPages}` })
  }
  buttons.push({ text: i18next.t('back'), callback_data: 'back' })
  return buttons
}

export function scrollButtonsEventsToDay (currentPage: number | string, totalPages: number): Array<{ text: string, callback_data: string }> {
  currentPage = +currentPage
  const buttons = []
  if (currentPage > 1) {
    buttons.push({ text: i18next.t('toHome'), callback_data: `scrollEventToDay:${1}` })
    buttons.push({ text: i18next.t('left'), callback_data: `scrollEventToDay:${currentPage - 1}` })
  }
  if (currentPage < totalPages) {
    buttons.push({ text: i18next.t('right'), callback_data: `scrollEventToDay:${currentPage + 1}` })
    buttons.push({ text: i18next.t('toEnd'), callback_data: `scrollEventToDay:${totalPages}` })
  }
  buttons.push({ text: i18next.t('back'), callback_data: 'back' })
  return buttons
}

export function scrollButtonsSearchedIssues (currentPage: number | string, totalPages: number, task: string): Array<{ text: string, callback_data: string }> {
  currentPage = +currentPage
  const buttons = []
  if (currentPage > 1) {
    buttons.push({ text: i18next.t('toHome'), callback_data: `scrollSearchedIssues:${1}task:${task}` })
    buttons.push({ text: i18next.t('left'), callback_data: `scrollSearchedIssues:${currentPage - 1}task:${task}` })
  }
  if (currentPage < totalPages) {
    buttons.push({ text: i18next.t('right'), callback_data: `scrollSearchedIssues:${currentPage + 1}task:${task}` })
    buttons.push({ text: i18next.t('toEnd'), callback_data: `scrollSearchedIssues:${totalPages}task:${task}` })
  }
  buttons.push({ text: i18next.t('back'), callback_data: 'back' })
  return buttons
}

// scrollThingsButtons
export function scrollThoughtsButtons (currentPage: number | string, totalPages: number): Array<{ text: string, callback_data: string }> {
  currentPage = +currentPage
  const buttons = []
  if (currentPage > 1) {
    buttons.push({ text: i18next.t('toHome'), callback_data: `scrollThoughts:${1}` })
    buttons.push({ text: i18next.t('left'), callback_data: `scrollThoughts:${currentPage - 1}` })
  }
  if (currentPage < totalPages) {
    buttons.push({ text: i18next.t('right'), callback_data: `scrollThoughts:${currentPage + 1}` })
    buttons.push({ text: i18next.t('toEnd'), callback_data: `scrollThoughts:${totalPages}` })
  }
  buttons.push({ text: i18next.t('back'), callback_data: 'back' })
  return buttons
}

export function scrollTensionsButtons (currentPage: number | string, totalPages: number): Array<{ text: string, callback_data: string }> {
  currentPage = +currentPage
  const buttons = []
  if (currentPage > 1) {
    buttons.push({ text: i18next.t('toHome'), callback_data: `scrollTensions:${1}` })
    buttons.push({ text: i18next.t('left'), callback_data: `scrollTensions:${currentPage - 1}` })
  }
  if (currentPage < totalPages) {
    buttons.push({ text: i18next.t('right'), callback_data: `scrollTensions:${currentPage + 1}` })
    buttons.push({ text: i18next.t('toEnd'), callback_data: `scrollTensions:${totalPages}` })
  }
  buttons.push({ text: i18next.t('back'), callback_data: 'back' })
  return buttons
}

export function scrollDriversButtons (currentPage: number | string, totalPages: number): Array<{ text: string, callback_data: string }> {
  currentPage = +currentPage
  const buttons = []
  if (currentPage > 1) {
    buttons.push({ text: i18next.t('toHome'), callback_data: `scrollDrivers:${1}` })
    buttons.push({ text: i18next.t('left'), callback_data: `scrollDrivers:${currentPage - 1}` })
  }
  if (currentPage < totalPages) {
    buttons.push({ text: i18next.t('right'), callback_data: `scrollDrivers:${currentPage + 1}` })
    buttons.push({ text: i18next.t('toEnd'), callback_data: `scrollDrivers:${totalPages}` })
  }
  buttons.push({ text: i18next.t('back'), callback_data: 'back' })
  return buttons
}

export function scrollProposalsButtons (currentPage: number | string, totalPages: number): Array<{ text: string, callback_data: string }> {
  currentPage = +currentPage
  const buttons = []
  if (currentPage > 1) {
    buttons.push({ text: i18next.t('toHome'), callback_data: `scrollProposals:${1}` })
    buttons.push({ text: i18next.t('left'), callback_data: `scrollProposals:${currentPage - 1}` })
  }
  if (currentPage < totalPages) {
    buttons.push({ text: i18next.t('right'), callback_data: `scrollProposals:${currentPage + 1}` })
    buttons.push({ text: i18next.t('toEnd'), callback_data: `scrollProposals:${totalPages}` })
  }
  buttons.push({ text: i18next.t('back'), callback_data: 'back' })
  return buttons
}

export function scrollSprintsButtons (currentPage: number | string, totalPages: number): Array<{ text: string, callback_data: string }> {
  currentPage = +currentPage
  const buttons = []
  if (currentPage > 1) {
    buttons.push({ text: i18next.t('toHome'), callback_data: `scrollSprints:${1}` })
    buttons.push({ text: i18next.t('left'), callback_data: `scrollSprints:${currentPage - 1}` })
  }
  if (currentPage < totalPages) {
    buttons.push({ text: i18next.t('right'), callback_data: `scrollSprints:${currentPage + 1}` })
    buttons.push({ text: i18next.t('toEnd'), callback_data: `scrollSprints:${totalPages}` })
  }
  buttons.push({ text: i18next.t('back'), callback_data: 'back' })
  return buttons
}

// ---------------------------------scroll buttons zone off--------------------------

// -------------------------------choice buttons zone-------------------------------
export function priorityButtons (): { reply_markup: InlineKeyboardMarkup } {
  const button = {
    reply_markup: {
      inline_keyboard: [
        [{ text: i18next.t('highPriority'), callback_data: `setPriority:${Priority.HighPriority}` }],
        [{ text: i18next.t('mediumPriority'), callback_data: `setPriority:${Priority.MediumPriority}` }],
        [{ text: i18next.t('lowPriority'), callback_data: `setPriority:${Priority.LowPriority}` }],
        [{ text: i18next.t('defaulPriority'), callback_data: `setPriority:${Priority.MediumPriority}` }],
        [{ text: i18next.t('cancel'), callback_data: 'cancelIssue' }]
      ],
      resize_keyboard: true
    }
  }
  return button
}

export function gradeTensionButtons (): { reply_markup: InlineKeyboardMarkup } {
  const button = {
    reply_markup: {
      inline_keyboard: [
        [{ text: i18next.t('positive'), callback_data: `gradeTension:${Grade.positive}` }],
        [{ text: i18next.t('neutral'), callback_data: `gradeTension:${Grade.neutral}` }],
        [{ text: i18next.t('negative'), callback_data: `gradeTension:${Grade.negative}` }],
        [{ text: i18next.t('cancel'), callback_data: 'cancel' }]
      ],
      resize_keyboard: true
    }
  }
  return button
}

export function stateDriverButtons (): { reply_markup: InlineKeyboardMarkup } {
  const button = {
    reply_markup: {
      inline_keyboard: [
        [{ text: i18next.t('need'), callback_data: `stateDriver:${DriverState.need}` }],
        [{ text: i18next.t('goal'), callback_data: `stateDriver:${DriverState.goal}` }],
        [{ text: i18next.t('satisfied'), callback_data: `stateDriver:${DriverState.satisfied}` }],
        [{ text: i18next.t('irrelevant'), callback_data: `stateDriver:${DriverState.irrelevant}` }],
        [{ text: i18next.t('cancel'), callback_data: 'cancel' }]

      ],
      resize_keyboard: true
    }
  }
  return button
}

export function gradeDriverButtons (): { reply_markup: InlineKeyboardMarkup } {
  const button = {
    reply_markup: {
      inline_keyboard: [
        [{ text: i18next.t('positive'), callback_data: `gradeDriver:${Grade.positive}` }],
        [{ text: i18next.t('neutral'), callback_data: `gradeDriver:${Grade.neutral}` }],
        [{ text: i18next.t('negative'), callback_data: `gradeDriver:${Grade.negative}` }],
        [{ text: i18next.t('cancel'), callback_data: 'cancel' }]
      ],
      resize_keyboard: true
    }
  }
  return button
}

export function statusProposalButtons (): { reply_markup: InlineKeyboardMarkup } {
  const button = {
    reply_markup: {
      inline_keyboard: [
        [{ text: i18next.t('new'), callback_data: `proposalStatus:${ProposalStatus.new}` }],
        [{ text: i18next.t('accepted'), callback_data: `proposalStatus:${ProposalStatus.accepted}` }],
        [{ text: i18next.t('holded'), callback_data: `proposalStatus:${ProposalStatus.holded}` }],
        [{ text: i18next.t('rejected'), callback_data: `proposalStatus:${ProposalStatus.rejected}` }],
        [{ text: i18next.t('ceased'), callback_data: `proposalStatus:${ProposalStatus.ceased}` }],
        [{ text: i18next.t('review'), callback_data: `proposalStatus:${ProposalStatus.review}` }],
        [{ text: i18next.t('cancel'), callback_data: 'cancel' }]
      ],
      resize_keyboard: true
    }
  }
  return button
}

export function yesNoButtons (): { reply_markup: InlineKeyboardMarkup } {
  const button = {
    reply_markup: {
      inline_keyboard: [
        [{ text: i18next.t('YES'), callback_data: 'yes' }],
        [{ text: i18next.t('NO'), callback_data: 'cancel' }]
      ],
      resize_keyboard: true
    }
  }
  return button
}
