import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import i18next from '../../localization/phrasesRus'
import { type UserSessionTension } from '../../types/tensions'
import config from '../../config/config'
import { Menu } from '../../buttons/markups'

export async function setTension (ctx: Context, userSessionsTension: UserSessionTension): Promise<void> {
  try {
    const client = await createGraphQLClient()
    const query: string = gql`
        mutation($tension: TensionInput!){
            createTension(tension: $tension) {
              id  
              name
            domain {
                id
            }
            tensionMark
            description
            }
        }`
    const variables = {
      tension: {
        name: userSessionsTension.tensionName,
        tensionMark: userSessionsTension.tensionMark,
        description: userSessionsTension.description,
        domainId: userSessionsTension.domainId
      }
    }
    const data: { createTension?: { id: string, name: string, tensionMark: string, description: string, domain: { id: string } } } = await client.request(query, variables)
    const tension = data.createTension
    const tensionName = tension?.name
    const tensionId: string = tension?.id ?? ''
    const domainId: string = tension?.domain.id ?? ''
    const link = config.s3appLink + '/domains/' + domainId + '/tensions/' + tensionId
    const linkName = `${i18next.t('tensionCreated')} <a href="${link}">${tensionName}</a>`

    ctx.editMessageText(linkName, {
      link_preview_options: {
        is_disabled: true
      },
      parse_mode: 'HTML',
      ...Menu()
    })
    console.log(data)
  } catch (error) {
    console.log(error)
  }
}
