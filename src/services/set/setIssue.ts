import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import i18next from '../../localization/phrasesRus'
import { type UserSession } from '../../types/task'
import { Menu } from '../../buttons/markups'
import config from '../../config/config'

export async function setIssue (ctx: Context, userSessions: UserSession): Promise<void> {
  try {
    const client = await createGraphQLClient()
    const query: string = gql`
          mutation($issue: IssueInput!){
            createIssue(issue: $issue) {
              id
              name
              stage {
                id
              }
              domain {
                id
              }
              author {
                id
                name
              }
              priority
            }
        }`
    const variables = {
      issue: {
        domainId: userSessions.domainId,
        stageId: userSessions.stageId,
        name: userSessions.issueName,
        priority: userSessions.priority
      }
    }
    const data: { createIssue?: { id: string, name: string, stage: { id: string }, domain: { id: string } } } = await client.request(query, variables)
    const issue = data.createIssue
    const issueName = issue?.name
    const issueId: string = issue?.id ?? ''
    const domainId: string = issue?.domain.id ?? ''
    const link = config.s3appLink + '/domains/' + domainId + '/issues/' + issueId
    const linkName = `${i18next.t('issueCreated')} <a href="${link}">${issueName}</a>`

    ctx.editMessageText(linkName, {
      link_preview_options: {
        is_disabled: true
      },
      parse_mode: 'HTML',
      ...Menu()

    })
    console.log(data)
  } catch (error) {
    console.log(error)
  }
}
