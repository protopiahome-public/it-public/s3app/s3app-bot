import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import i18next from '../../localization/phrasesRus'
import { type UserSessionDriver } from '../../types/drivers'
import config from '../../config/config'
import { type DriverState } from '../../enums/enum'
import { Menu } from '../../buttons/markups'

export async function setDriver (ctx: Context, userSessionsDriver: UserSessionDriver): Promise<void> {
  try {
    const client = await createGraphQLClient()
    const query: string = gql`
        mutation($driver: DriverInput!){
            createDriver(driver: $driver) {
                id
                name
                description
                driverEffect
                driverState
                domain {
                    id
                }
            }
        }`
    const variables = {
      driver: {
        name: userSessionsDriver.driverName,
        description: userSessionsDriver.description,
        driverEffect: userSessionsDriver.driverEffect,
        driverState: userSessionsDriver.driverState,
        domainId: userSessionsDriver.domainId
      }
    }
    const data: { createDriver?: { id: string, name: string, description: string, driverEffect: string, driverState: DriverState, domain: { id: string } } } = await client.request(query, variables)
    const driver = data.createDriver
    const driverName = driver?.name
    const driverId: string = driver?.id ?? ''
    const domainId: string = driver?.domain.id ?? ''
    const link = config.s3appLink + '/domains/' + domainId + '/drivers/' + driverId
    const linkName = `${i18next.t('driverCreated')} <a href="${link}">${driverName}</a>`

    ctx.editMessageText(linkName, {
      link_preview_options: {
        is_disabled: true
      },
      parse_mode: 'HTML',
      ...Menu()
    })
    console.log(data)
  } catch (error) {
    console.log(error)
  }
}
