import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import i18next from '../../localization/phrasesRus'
import { type UserSessionThought } from '../../types/thoughts'
import config from '../../config/config'
import { Menu } from '../../buttons/markups'

export async function setThought (ctx: Context, userSessionsThought: UserSessionThought): Promise<void> {
  try {
    const client = await createGraphQLClient()
    const query: string = gql`
        mutation($thought: ThoughtInput!){
            createThought(thought: $thought) {
                id
                name
                domain {
                    id
                }
            }
        }`
    const variables = {
      thought: {
        name: userSessionsThought.thoughtName,
        domainId: userSessionsThought.domainId
      }
    }
    const data: { createThought?: { id: string, name: string, domain: { id: string } } } = await client.request(query, variables)
    const thought = data.createThought
    const thoughtName = thought?.name
    const thoughtId: string = thought?.id ?? ''
    const domainId: string = thought?.domain.id ?? ''
    const link = config.s3appLink + '/domains/' + domainId + '/thoughts/' + thoughtId
    const linkName = `${i18next.t('thoughtCreated')} <a href="${link}">${thoughtName}</a>`

    ctx.editMessageText(linkName, {
      link_preview_options: {
        is_disabled: true
      },
      parse_mode: 'HTML',
      ...Menu()
    })
    console.log(data)
  } catch (error) {
    console.log(error)
  }
}
