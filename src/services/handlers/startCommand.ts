import { type Context } from 'telegraf'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from './checkForChatId'
import { menuButtons } from '../../buttons/markups'

export function start (ctx: Context): void {
  const newLine: string = '\n'
  if (isChatIdAllowed(ctx)) {
    const username: string | undefined = ctx.from?.username
    ctx.reply(i18next.t('StartMessage', { username, newLine }), menuButtons())
  } else {
    const chatId = ctx.chat?.id.toString()
    ctx.reply(i18next.t('errorChatId', { chatId, newLine }))
  }
}
