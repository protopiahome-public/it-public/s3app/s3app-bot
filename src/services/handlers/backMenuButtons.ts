import { type Context } from 'telegraf'
import { menuButtons } from '../../buttons/markups'
import i18next from 'i18next'

export const menu = async (ctx: Context, flag: string): Promise<void> => {
  if (flag === 'back') {
    await ctx.editMessageText(i18next.t('menu'), menuButtons())
  }
}
