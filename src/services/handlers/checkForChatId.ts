import { type Context } from 'telegraf'
import config from '../../config/config'

export function isChatIdAllowed (ctx: Context): boolean {
  const chatId = ctx.chat?.id.toString()
  const allowedChatIds = config.allowedChatIds
  if (ctx.from && ctx.chat && chatId) {
    return !!((allowedChatIds.length === 0 || allowedChatIds.includes(chatId)))
  } else {
    return false
  }
}
