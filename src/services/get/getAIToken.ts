import fetch from 'node-fetch'
import config from '../../config/config'
import i18next from 'i18next'
import { v4 as uuidv4 } from 'uuid'
import https from 'https'
import { Menu } from '../../buttons/markups'

const agent = new https.Agent({
  // ca: fs.readFileSync(`${config.certPath}`)
  rejectUnauthorized: false
})

export async function getAccessToken (): Promise<string | null> {
  try {
    const authKey = config.gigaChatApiKey
    const response = await fetch('https://ngw.devices.sberbank.ru:9443/api/v2/oauth', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json',
        Authorization: `Basic ${authKey}`,
        RqUID: uuidv4()
      },
      body: 'scope=GIGACHAT_API_PERS',
      agent
    })

    if (!response.ok) {
      console.error(`Failed to obtain access token: ${response.status} ${response.statusText}`)
      return null
    }

    const data = await response.json()
    return data.access_token ? data.access_token.toString() : null
  } catch (error) {
    console.error(i18next.t('errorAccesToken'), error, Menu())
    return null
  }
}
