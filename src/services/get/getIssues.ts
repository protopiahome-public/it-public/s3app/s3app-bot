import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import moment from 'moment'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from '../handlers/checkForChatId'
import { menuButtons, scrollButtons } from '../../buttons/markups'
import config from '../../config/config'
import { type issueType } from '../../types/issues'

export async function getIssues (ctx: Context, currentPage: number | string, tasksPerPage = 4): Promise<void> {
  currentPage = +currentPage
  if (isChatIdAllowed(ctx)) {
    try {
      const client = await createGraphQLClient()
      const query: string = gql`
           query {
            getIssues (page: 1 perPage: 100000) {
               id,
               domain {
                id,
                name
               },
               createdAt,
               name,
               author {
                name
               },
               executor {
                name
               },
            }
           }`
      const data: { getIssues: issueType[] } = await client.request(query)
      const issues: issueType[] = data.getIssues
      issues.sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())
      function formatDate (createdAt: string): string {
        const date = moment(createdAt)
        const formattedDate = date.format('DD.MM.YYYY HH:mm')
        return formattedDate
      }
      const newLine: string = '\n'
      let message: string = i18next.t('MessageGetListIssues', { newLine })
      const totalPages = Math.ceil(issues.length / tasksPerPage)
      const startIndex = (currentPage - 1) * tasksPerPage
      const slicedIssues = issues.slice(startIndex, startIndex + tasksPerPage)

      slicedIssues.forEach((issue) => {
        const issueAuthor: string = issue.author !== null ? issue.author.name : i18next.t('notAuthor')
        const issueExecutor: string = issue.executor !== null ? issue.executor.name : i18next.t('notExecutor')
        const dateText: string = formatDate(issue.createdAt)
        message += `<a href="${config.s3appLink}/domains/${issue.domain !== null ? issue.domain.id : 'aeafdcb7-6c48-4da1-a33e-7e184b09416d'}/issues/${issue.id}">${issue.name}</a>`
        message += i18next.t('MessageGetListIssuesData', { issue, dateText, newLine, issueAuthor, issueExecutor })
      })

      message += i18next.t('endMessageOfListIssue', { newLine, currentPage, totalPages })

      await ctx.editMessageText(message, {
        link_preview_options: {
          is_disabled: true
        },
        parse_mode: 'HTML',
        reply_markup: {
          inline_keyboard: [scrollButtons(currentPage, totalPages)]
        }
      }
      )
    } catch (error) {
      console.error(error)
      ctx.reply(i18next.t('ErrorGetIssueList'), menuButtons())
    }
  } else {
    const chatId = ctx.chat?.id.toString()
    const newLine: string = '\n'
    ctx.reply(i18next.t('errorChatId', { chatId, newLine }), menuButtons())
  }
}
