import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from '../handlers/checkForChatId'
import { type StageType, type DomainStagesType } from '../../types/stages'
import { stagesButtons } from '../../buttons/markups'

export async function getStagesList (ctx: Context, domainId: string): Promise<void> {
  if (isChatIdAllowed(ctx)) {
    try {
      const client = await createGraphQLClient()
      const query: string = gql`
                query ($domainId: ID!) {
                    getDomain(id: $domainId)  {
                        id
                        name
                        stages {
                            name
                            id
                        } 
                    }
                }
            `
      const data = await client.request <{
        getDomain: DomainStagesType
      }>(query, { domainId })
      const domain: DomainStagesType = data.getDomain
      const stages: StageType[] = domain.stages
      const stagesList: any = stages.map((stage) => (
        [{
          text: stage.name,
          callback_data: 'setStage:' + stage.id
        }]
      ))
      ctx.editMessageText(i18next.t('stageChoice'), stagesButtons(stagesList))
    } catch (error) {
      console.error(error)
    }
  } else {
    ctx.editMessageText(i18next.t('errorChatId'))
  }
}
