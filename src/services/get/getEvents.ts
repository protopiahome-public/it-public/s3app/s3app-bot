import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import moment from 'moment'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from '../handlers/checkForChatId'
import { menuButtons, scrollButtonsEvents } from '../../buttons/markups'
import { type eventsType } from '../../types/events'

export async function getEvents (ctx: Context, currentPage: number | string, tasksPerPage = 4): Promise<void> {
  currentPage = +currentPage
  if (isChatIdAllowed(ctx)) {
    try {
      const client = await createGraphQLClient()
      const query: string = gql`
        query {
            getEvents{
            id,
            name,
            domain{
                id,
                name
            },
            startDate,
            endDate,
            participants{
              name
            }
            }
        }`
      const data: { getEvents: eventsType[] } = await client.request(query)
      const events: eventsType[] = data.getEvents
      console.log(data)

      function formatDate (Date: string): string {
        const date = moment(Date)
        const formattedDate = date.format('DD.MM.YYYY HH:mm')
        return formattedDate
      }
      const newLine: string = '\n'
      let message: string = i18next.t('EventsList', { newLine })
      const totalPages = Math.ceil(events.length / tasksPerPage)
      const startIndex = (currentPage - 1) * tasksPerPage
      const slicedEvents = events.slice(startIndex, startIndex + tasksPerPage)

      slicedEvents.forEach((event) => {
        const startTime: string = formatDate(event.startDate)
        const endTime: string = formatDate(event.endDate)
        const participants: string = event.participants && event.participants.length > 0 ? event.participants.map(participant => participant.name).join(', ') : i18next.t('notParticipants')

        message += i18next.t('MessageGetEvents', { event, newLine, startTime, endTime, participants })
        console.log('Участники', participants)
      })

      message += i18next.t('endMessageOfListIssue', { newLine, currentPage, totalPages })

      await ctx.editMessageText(message, {
        link_preview_options: {
          is_disabled: true
        },
        parse_mode: 'HTML',
        reply_markup: {
          inline_keyboard: [scrollButtonsEvents(currentPage, totalPages)]
        }
      }
      )
    } catch (error) {
      console.error(error)
      ctx.reply(i18next.t('ErrorGetEvents'), menuButtons())
    }
  } else {
    const chatId = ctx.chat?.id.toString()
    const newLine: string = '\n'
    ctx.reply(i18next.t('errorChatId', { chatId, newLine }), menuButtons())
  }
}
