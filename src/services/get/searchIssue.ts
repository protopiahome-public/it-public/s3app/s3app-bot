import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import moment from 'moment'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from '../handlers/checkForChatId'
import { menuButtons, scrollButtonsSearchedIssues } from '../../buttons/markups'
import config from '../../config/config'
import { type issueType } from '../../types/issues'
import { IssueSearch } from '../../enums/enum'
import { type UserSession } from '../../types/search'

const userSessions: Record<number, UserSession> = {}

export async function searchIssues (ctx: Context, task: string, flag: string, currentPage: number | string, tasksPerPage = 4): Promise<void> {
  const telegramId: number = Number(ctx.from?.id)

  let messageId: number | undefined
  let chatId: number | undefined
  if (flag === IssueSearch.firstPage) {
    const sentMessage = await ctx.reply(i18next.t('iSearch'))
    messageId = sentMessage.message_id
    chatId = sentMessage.chat.id
    userSessions[telegramId] = {
      chatId,
      messageId
    }
  } else if (flag === IssueSearch.currentPage) {
    messageId = userSessions[telegramId]?.messageId
    chatId = userSessions[telegramId]?.chatId
  }

  const requestPerPage = 10
  currentPage = +currentPage

  if (isChatIdAllowed(ctx)) {
    try {
      const client = await createGraphQLClient()
      const query: string = gql`
        query GetIssues($page: Int!, $perPage: Int!, $filter: String!) {
         getIssues(page: $page, perPage: $perPage, filter: { name: $filter }) {
            id,
            domain {
             id,
             name
            },
            createdAt,
            name,
            author {
             name
            },
            executor {
             name
            },
         }
        }`

      const variables = {
        page: currentPage,
        perPage: requestPerPage,
        filter: task
      }

      const data: { getIssues: issueType[] } = await client.request(query, variables)
      const issues: issueType[] = data.getIssues

      if (issues.length !== 0) {
        let message: string = i18next.t('searchedIssues', { newLine: '\n' })
        const totalPages = Math.ceil(issues.length / tasksPerPage)

        issues.forEach((issue) => {
          const issueAuthor: string = issue.author !== null ? issue.author.name : i18next.t('notAuthor')
          const issueExecutor: string = issue.executor !== null ? issue.executor.name : i18next.t('notExecutor')
          const dateText: string = moment(issue.createdAt).format('DD.MM.YYYY HH:mm')

          message += `<a href="${config.s3appLink}/domains/${issue.domain !== null ? issue.domain.id : 'aeafdcb7-6c48-4da1-a33e-7e184b09416d'}/issues/${issue.id}">${issue.name}</a>`
          message += i18next.t('MessageGetListIssuesData', { issue, dateText, newLine: '\n', issueAuthor, issueExecutor })
        })

        message += i18next.t('endMessageOfListIssue', { newLine: '\n', currentPage, totalPages })
        const encodeTask = Buffer.from(task, 'utf-8').toString('base64')

        await ctx.telegram.editMessageText(chatId, messageId, undefined, message, {
          parse_mode: 'HTML',
          reply_markup: {
            inline_keyboard: [scrollButtonsSearchedIssues(currentPage, totalPages, encodeTask)]
          }
        })
      } else {
        ctx.telegram.editMessageText(chatId, messageId, undefined, i18next.t('noSearch'), menuButtons())
      }
    } catch (error) {
      console.error(error)
      ctx.reply(i18next.t('ErrorGetIssueList'), menuButtons())
    }
  } else {
    const chatId = ctx.chat?.id.toString()
    ctx.reply(i18next.t('errorChatId', { chatId, newLine: '\n' }), menuButtons())
  }
}
