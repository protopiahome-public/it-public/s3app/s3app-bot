import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import moment from 'moment'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from '../handlers/checkForChatId'
import { menuButtons, scrollThoughtsButtons } from '../../buttons/markups'
import { type thoughtType } from '../../types/thoughts'

export async function getThoughts (ctx: Context, currentPage: number | string, tasksPerPage = 4): Promise<void> {
  currentPage = +currentPage
  if (isChatIdAllowed(ctx)) {
    try {
      const client = await createGraphQLClient()
      const query: string = gql`
        query{
            getThoughts {
            id,
            domain{
              id,
              name
            },
            name,
            authorUser{
                name
            },
            createdAt,
            updatedAt,    
            }
        }`
      const data: { getThoughts: thoughtType[] } = await client.request(query)
      const thoughts: thoughtType[] = data.getThoughts
      thoughts.sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())
      function formatDate (inputDate: string): string {
        const date = moment(inputDate)
        const formattedDate = date.format('DD.MM.YYYY HH:mm')
        return formattedDate
      }
      const newLine: string = '\n'
      let message: string = i18next.t('thoughts', { newLine })
      const totalPages = Math.ceil(thoughts.length / tasksPerPage)
      const startIndex = (currentPage - 1) * tasksPerPage
      const slicedThoughts = thoughts.slice(startIndex, startIndex + tasksPerPage)

      slicedThoughts.forEach((thought) => {
        const thoughtAuthor: string = thought.authorUser !== null ? thought.authorUser.name : i18next.t('notAuthor')
        const dateText: string = formatDate(thought.createdAt)
        const updateText: string = thought.updatedAt !== null ? formatDate(thought.updatedAt) : dateText
        message += i18next.t('getThoughts', { thought, dateText, updateText, newLine, thoughtAuthor })
      })

      message += i18next.t('endMessageOfListIssue', { newLine, currentPage, totalPages })

      await ctx.editMessageText(message, {
        link_preview_options: {
          is_disabled: true
        },
        parse_mode: 'HTML',
        reply_markup: {
          inline_keyboard: [scrollThoughtsButtons(currentPage, totalPages)]
        }
      }
      )
    } catch (error) {
      console.error(error)
      ctx.reply(i18next.t('ErrorGetThoughts'), menuButtons())
    }
  } else {
    const chatId = ctx.chat?.id.toString()
    const newLine: string = '\n'
    ctx.reply(i18next.t('errorChatId', { chatId, newLine }), menuButtons())
  }
}
