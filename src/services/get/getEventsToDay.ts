import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import moment from 'moment'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from '../handlers/checkForChatId'
import { menuButtons, scrollButtonsEventsToDay } from '../../buttons/markups'
import { type eventsType } from '../../types/events'

export async function getEventsToDay (ctx: Context, currentPage: number | string, tasksPerPage = 4): Promise<void> {
  currentPage = +currentPage
  if (isChatIdAllowed(ctx)) {
    try {
      const client = await createGraphQLClient()
      const query: string = gql`
        query {
            getEvents{
            id,
            name,
            domain{
                id,
                name
            },
            startDate,
            endDate,
            participants{
              name
            }
            }
        }`
      const data: { getEvents: eventsType[] } = await client.request(query)
      const events: eventsType[] = data.getEvents

      function formatDate (Date: string): string {
        const date = moment(Date)
        const formattedDate = date.format('DD.MM.YYYY HH:mm')
        return formattedDate
      }

      const newLine: string = '\n'
      let message: string = i18next.t('EventsList', { newLine })

      let cnt: number = 0
      const today = new Date()
      const fToday = moment(today).format('YYYY-MM-DD')

      events.forEach(event => {
        const startTime: string = formatDate(event.startDate)
        const endTime: string = formatDate(event.endDate)
        const participants: string = event.participants && event.participants.length > 0 ? event.participants.map(participant => participant.name).join(', ') : i18next.t('notParticipants')

        const start = moment(event.startDate).format('YYYY-MM-DD')
        const end = moment(event.endDate).format('YYYY-MM-DD')

        if (fToday >= start && fToday <= end) {
          cnt += 1
          message += i18next.t('MessageGetEvents', { event, newLine, startTime, endTime, participants })
        }
      })

      const totalPages = Math.ceil(cnt / tasksPerPage)

      if (cnt !== 0) {
        message += i18next.t('endMessageOfListIssue', { newLine, currentPage, totalPages })
        await ctx.editMessageText(message, {
          link_preview_options: {
            is_disabled: true
          },
          parse_mode: 'HTML',
          reply_markup: {
            inline_keyboard: [scrollButtonsEventsToDay(currentPage, totalPages)]
          }
        }
        )
      } else {
        ctx.reply(i18next.t('ErrorGetEvents'), menuButtons())
      }
    } catch (error) {
      console.error(error)
      ctx.reply(i18next.t('ErrorGetEvents'), menuButtons())
    }
  } else {
    const chatId = ctx.chat?.id.toString()
    const newLine: string = '\n'
    ctx.reply(i18next.t('errorChatId', { chatId, newLine }), menuButtons())
  }
}
