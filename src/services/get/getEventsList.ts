import { type Telegraf } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import moment from 'moment'
import i18next from '../../localization/phrasesRus'
import { type eventsType } from '../../types/events'
import config from '../../config/config'

export async function getEventsList (bot: Telegraf): Promise<any> {
  try {
    const client = await createGraphQLClient()
    const query: string = gql`
        query {
            getEvents{
            id,
            name,
            domain{
                id,
                name
            },
            startDate,
            endDate,
            participants{
              name
            }
          }
        }`
    const data: { getEvents: eventsType[] } = await client.request(query)
    const events: eventsType[] = data.getEvents
    function formatDate (Date: string): string {
      const date = moment(Date)
      const formattedDate = date.format('DD.MM.YYYY HH:mm')
      return formattedDate
    }

    const newLine: string = '\n'
    let message: string = '<b>' + i18next.t('getEvents', { newLine }) + '</b>'
    let cnt: number = 0
    const today = new Date()
    const fToday = moment(today).format('YYYY-MM-DD')

    events.forEach(event => {
      const startTime: string = formatDate(event.startDate)
      const endTime: string = formatDate(event.endDate)
      const participants: string = event.participants && event.participants.length > 0 ? event.participants.map(participant => participant.name).join(', ') : i18next.t('notParticipants')

      const start = moment(event.startDate).format('YYYY-MM-DD')
      const end = moment(event.endDate).format('YYYY-MM-DD')

      if (fToday >= start && fToday <= end) {
        cnt += 1
        message += i18next.t('MessageGetEvents', { event, newLine, startTime, endTime, participants })
      }
    })
    if (cnt !== 0) {
      return message
    } else {
      message = ''
      return message
    }
  } catch (error) {
    console.error(error)
    bot.telegram.sendMessage(i18next.t('ErrorGetEvents'), config.chatID)
  }
}
