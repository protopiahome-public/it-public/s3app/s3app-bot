import { type Context } from 'telegraf'
import { gql } from 'graphql-request'
import { createGraphQLClient } from '../handlers/apiRequest'
import moment from 'moment'
import i18next from '../../localization/phrasesRus'
import { isChatIdAllowed } from '../handlers/checkForChatId'
import { menuButtons, scrollTensionsButtons } from '../../buttons/markups'
import { type tensionType as tensiontType } from '../../types/tensions'

export async function getTensions (ctx: Context, currentPage: number | string, tasksPerPage = 4): Promise<void> {
  currentPage = +currentPage
  if (isChatIdAllowed(ctx)) {
    try {
      const client = await createGraphQLClient()
      const query: string = gql`
        query{
            getTensions {
            id,
            domain{
              id,
              name
            },
            name,
            authorUser{
                name
            },
            createdAt,
            updatedAt, 
            tensionMark   
            }
        }  
      `
      const data: { getTensions: tensiontType[] } = await client.request(query)
      const tensions: tensiontType[] = data.getTensions
      tensions.sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())
      function formatDate (inputDate: string): string {
        const date = moment(inputDate)
        const formattedDate = date.format('DD.MM.YYYY HH:mm')
        return formattedDate
      }
      const newLine: string = '\n'
      let message: string = i18next.t('tensions', { newLine })
      const totalPages = Math.ceil(tensions.length / tasksPerPage)
      const startIndex = (currentPage - 1) * tasksPerPage
      const slicedTensions = tensions.slice(startIndex, startIndex + tasksPerPage)

      slicedTensions.forEach((tension) => {
        const tensionAuthor: string = tension.authorUser !== null ? tension.authorUser.name : i18next.t('notAuthor')
        const dateText: string = formatDate(tension.createdAt)
        const updateText: string = tension.updatedAt !== null ? formatDate(tension.updatedAt) : dateText
        message += i18next.t('getTensions', { tension, dateText, updateText, newLine, tensionAuthor })
      })

      message += i18next.t('endMessageOfListIssue', { newLine, currentPage, totalPages })

      await ctx.editMessageText(message, {
        link_preview_options: {
          is_disabled: true
        },
        parse_mode: 'HTML',
        reply_markup: {
          inline_keyboard: [scrollTensionsButtons(currentPage, totalPages)]
        }
      }
      )
    } catch (error) {
      console.error(error)
      ctx.reply(i18next.t('ErrorGetTensions'), menuButtons())
    }
  } else {
    const chatId = ctx.chat?.id.toString()
    const newLine: string = '\n'
    ctx.reply(i18next.t('errorChatId', { chatId, newLine }), menuButtons())
  }
}
