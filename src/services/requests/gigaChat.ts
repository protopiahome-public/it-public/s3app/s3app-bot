import { type Context } from 'telegraf'
import fetch from 'node-fetch'
import { type GigaChatResponse, type UserSessionAI } from '../../types/requestAI'
import i18next from '../../localization/phrasesRus'
import { Menu } from '../../buttons/markups'
import { getAccessToken } from '../get/getAIToken'
import { v4 as uuidv4 } from 'uuid'
import https from 'https'

const agent = new https.Agent({
  // ca: fs.readFileSync(`${config.certPath}`)
  rejectUnauthorized: false
})

export async function aiRequest (ctx: Context, userSessionsAI: UserSessionAI, roleContent: string): Promise<string | undefined> {
  console.log('role content: ', roleContent)
  try {
    const accessToken = await getAccessToken()
    if (!accessToken) {
      throw new Error('Hi. Failed to obtain access token')
    }
    const response = await fetch('https://gigachat.devices.sberbank.ru/api/v1/chat/completions', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`,
        'X-Request-ID': uuidv4()
      },
      body: JSON.stringify({
        model: 'GigaChat',
        messages: [
          {
            role: 'system',
            content: roleContent
          },
          {
            role: 'user',
            content: userSessionsAI.requestText
          }
        ],
        stream: false,
        update_interval: 0,
        repetition_penalty: 1
      }),
      agent
    })

    const data: GigaChatResponse = await response.json() as GigaChatResponse
    const replyContent: string = data.choices?.[0]?.message?.content

    if (replyContent && roleContent === i18next.t('AiContentAssistent')) {
      await ctx.reply(replyContent, Menu())
    } else if (replyContent && roleContent === i18next.t('AiContentGetDate')) {
      console.log('replyContent---------->', replyContent)
      return replyContent
    } else {
      console.error('No valid response content found')
      await ctx.reply(i18next.t('errorAIRequest'), Menu())
    }
  } catch (error) {
    console.error(error)
    await ctx.reply(i18next.t('errorAIRequest'), Menu())
  }
}
