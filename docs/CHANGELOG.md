## [0.5.1](https://gitlab.com/s3app-team/s3app/s3app-bot/compare/v0.5.0...v0.5.1) (2024-04-14)


### Bug Fixes

* Correct output before add_issue command ([a72cc4f](https://gitlab.com/s3app-team/s3app/s3app-bot/commit/a72cc4f9f5aa7ee3a3647a441ff92d41732cce30))

# [0.5.0](https://gitlab.com/s3app-team/s3app/s3app-bot/compare/v0.4.0...v0.5.0) (2024-04-13)


### Features

* Add issue for domain from chat with bot ([62a5cd5](https://gitlab.com/s3app-team/s3app/s3app-bot/commit/62a5cd51a1a713f7a0eac5bcefcdceb7a7852c53))

# [0.4.0](https://gitlab.com/s3app-team/s3app/s3app-bot/compare/v0.3.0...v0.4.0) (2024-02-26)


### Features

* Навигация с помощью кнопок ([9e0db57](https://gitlab.com/s3app-team/s3app/s3app-bot/commit/9e0db574e9968d38f69a6f3ef767e120f691d53f))

# [0.3.0](https://gitlab.com/s3app-team/s3app/s3app-bot/compare/v0.2.1...v0.3.0) (2024-02-09)


### Features

* Использование бота в групповом чате и получение задач круга ([61ed524](https://gitlab.com/s3app-team/s3app/s3app-bot/commit/61ed5245c6a5b8cd9e02072dbc6db33406916326))

## [0.2.1](https://gitlab.com/s3app-team/s3app/s3app-bot/compare/v0.2.0...v0.2.1) (2024-01-31)


### Bug Fixes

* Длинные сообщения в telegram ([dc793da](https://gitlab.com/s3app-team/s3app/s3app-bot/commit/dc793da506209d1630ca549a6a32d79ae2ba97fc))

# [0.2.0](https://gitlab.com/s3app-team/s3app/s3app-bot/compare/v0.1.0...v0.2.0) (2024-01-23)


### Features

* deploy script ([311d7d3](https://gitlab.com/s3app-team/s3app/s3app-bot/commit/311d7d3b46ff0fc76e1e4a637beb985112df9ad4))
* Интернационализация и ограничение ответа по чатам ([3099381](https://gitlab.com/s3app-team/s3app/s3app-bot/commit/30993817a1235b2af4aa513689b4a958d351ce81))
