FROM node:18
WORKDIR /app
COPY package.json package.json
RUN npm install
COPY . .
COPY src/config/config.docker.ts src/config/config.ts
RUN npm run build
CMD ["npm", "start"]